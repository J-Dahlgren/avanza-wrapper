import Avanza from "avanza";

export class AvanzaExt extends Avanza {
    constructor() {
        super();
    }
    async getAccountBuyingPower(AccountId: string) {
        const overview = await this.getOverview();
        const account = overview.accounts.find(account => account.accountId === AccountId);
        if (!account) {
            throw new Error("Account not found");
        }
        return account.buyingPower;
    }
}